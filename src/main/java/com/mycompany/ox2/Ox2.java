/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.ox2;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Ox2 {

    private static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char turn = 'X';
    private static int row;
    private static Scanner sc = new Scanner(System.in);
    private static int col;
    private static int count = 0;

    public static void main(String[] args) {
        Welcome();
        while (true) {
            ShowTable();
            Showturn();
            inputRowCol();
            if (isfinish()) {
                ShowTable();
                showResult();
                break;
            }
            SwitchTurn();
        }
    }

    public static void Welcome() {
        System.out.println("Welcome to OX game");
    }

    private static void ShowTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void Showturn() {
        System.out.println("Turn " + turn);
    }

    private static void inputRowCol() {
        while (true) {
            System.out.print("please input(row,col)");
            if (sc.hasNextInt()) {
                row = sc.nextInt() - 1;
            } else {
                System.out.println("Please input number");
                sc.next();
                continue;
            }
            if (sc.hasNextInt()) {
                col = sc.nextInt() - 1;
            } else {
                System.out.println("Please input number");
                sc.next();
                continue;
            }

            if (row < 0 || row >= 3 || col < 0 || col >= 3) {
                System.out.println("Row and column must be between 1 and 3.");
                continue;
            }
            if (table[row][col] != '-') {
                System.out.println("Position already taken. Please choose another.");
                continue;
            }
            //       System.out.println(" " + row + " " + col);
            table[row][col] = turn;
            count++;
            break;
        }

    }

    private static void SwitchTurn() {
        if (turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }

    }

    private static boolean isfinish() {
        if (checkWin()) {
            return true;
        }
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    private static boolean checkWin() {
        if (checkRow()) {
            return true;
        }
        if (checkCol()) {
            return true;
        }
        if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkDraw() {
        if (count == 9) {
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        if (table[row][0] == turn && table[row][1] == turn && table[row][2] == turn) {
            return true;
        }
        return false;
    }

    private static boolean checkCol() {
        if (table[0][col] == turn && table[1][col] == turn && table[2][col] == turn) {
            return true;
        }
        return false;
    }

    private static boolean checkX() {
        if ((table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) || (table[0][2] == turn && table[1][1] == turn && table[2][0] == turn)) {
            return true;
        }
        return false;
    }

    private static void showResult() {
        if (checkWin()) {
            System.out.println(turn + " Win!!");
        }
        if (checkDraw()) {
            System.out.println("Draw!!");
        }
    }
}
